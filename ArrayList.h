//Karl Menzel
//Trinity University

#ifndef ARRAYLIST_H
#define ARRAYLIST_H

template<typename T>
class ArrayList{        
    private:
        int length, last;
        T* arr;

        void growArray(){
            length *= 2;
            T* temp = new T[length];
            for(int i = 0; i < last; i++)
                temp[i] = arr[i];
            delete[] arr;
            arr = temp;
        }

    public:
        typedef T value_type;
                  
        ArrayList(){
            length = 10;
            last = 0;
            arr = new T[length];
        }
        
        ArrayList(const ArrayList &that){
            length = that.length;
            last = that.last;
            arr = new T[length];
            for(int i = 0; i < last; i++)
                arr[i] = that.arr[i];
        }
        
        ArrayList<T> &operator=(const ArrayList<T> &al){
            length = al.length;
            last = al.last;
            delete[] arr;
            arr = new T[length];
            for(int i = 0; i < last; i++)
                arr[i] = al.arr[i];
            
            return *this;
        }
        
        ~ArrayList(){
            delete[] arr;
        }
        
        void push_back(const T &t){
            if(last >= length)
                growArray();
            arr[last] = t;
            last++;
        }
        
        void pop_back(){
            if(last > 0)
                last--;
        }
        
        int size() const{
            return last;
        }
        
        void clear(){
            last = 0;
        }

        void insert(const T &t,int index){
            if(last >= length)
                growArray();
            for(int i = last; i >= index; i--)
                arr[i] = arr[i-1];
            arr[index] = t;
            last++;
        }
        
        const T &operator[](int index) const{
            return arr[index];
        }
        
        T &operator[](int index){
            return arr[index];
        }
        
        void remove(int index){
            for(int i = index; i < last; i++)
                arr[i] = arr[i+1];
            last--;
        }

        class iterator{
            private:
                T *point;
            public:
                iterator(T *l){point  = l;}
                iterator(){point = nullptr;}
                iterator(const iterator &i){point = i.point;}
                T &operator*(){return *point;}
                bool operator==(const iterator &i) const{return (point == i.point);}
                bool operator!=(const iterator &i) const{return (point != i.point);}
                iterator &operator=(const iterator &i){point = i.point;}
                iterator &operator++(){point++; return *this;}
                iterator &operator--(){point--; return *this;}
                iterator operator++(int){point++; return iterator(point-1);}
                iterator operator--(int){point--; return iterator(point+1);}
        };
        
        class const_iterator {
            private:
                T *point;
            public:
                const_iterator(T *l){point = l;}
                const_iterator(){point = nullptr;}
                const_iterator(const const_iterator &i){point = i.point;}
                T &operator*(){return *point;}
                bool operator==(const const_iterator &i) const{return (point == i.point);}
                bool operator!=(const const_iterator &i) const{return (point != i.point);}
                const_iterator &operator=(const const_iterator &i){point = i.point;}
                const_iterator &operator++(){point++; return *this;}
                const_iterator &operator--(){point--; return *this;}
                const_iterator operator++(int){point++; return const_iterator(point-1);}
                const_iterator operator--(int){point--; return const_iterator(point+1);}
        };
       

        iterator begin(){return iterator(&arr[0]);}
        iterator end(){return iterator(&arr[last]);}
        
        const_iterator begin() const{return cbegin();}
        const_iterator end() const{return cend();}
        
        const_iterator cbegin() const{return const_iterator(&arr[0]);}
        const_iterator cend() const{return const_iterator(&arr[last]);}
};

#endif
