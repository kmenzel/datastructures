//Karl Menzel
//Trinity University

#ifndef FILELINKEDLIST_H
#define FILELINKEDLIST_H

#include <cstdio>
#include <string>

#define INTSIZE 4
#define SENTINEL 8

template<typename T>
class FileLinkedList {
    struct Node{
        T data;
        int prev;
        int next;
    };
    FileLinkedList(const FileLinkedList<T> &that) = delete;
    FileLinkedList<T> operator=(const FileLinkedList<T> &that) = delete;
    
    private:
        FILE *f;
        int length;
        const int TSIZE = sizeof(T);
        
        Node read(int address) const{
            return Node {getData(address), getPrev(address), getNext(address)};
        }

        int getAddress(int index) const{
            int currentSpot = SENTINEL;
            for(int i = -1; i < index; i++)
                currentSpot = getNext(currentSpot);
            return currentSpot;
        }

        T getData(int address) const{
            T ret;
            fseek(f, address, SEEK_SET);
            fread(&ret, TSIZE, 1, f);
            return ret;
        }

        int getPrev(int address) const{
            int spot = address + TSIZE;
            fseek(f, spot, SEEK_SET);
            int ret;
            fread(&ret, INTSIZE, 1, f);
            return ret;
        }
        int getNext(int address) const{
            int spot = address + TSIZE + INTSIZE;
            fseek(f, spot, SEEK_SET);
            int ret;
            fread(&ret, INTSIZE, 1, f);
            return ret;
        }
        void writeData(const T *t, int address){
            fseek(f, address, SEEK_SET);
            fwrite(t, TSIZE, 1, f);
        }
        void writeNext(int *newNext, int address){
            fseek(f, address + TSIZE + INTSIZE, SEEK_SET);
            fwrite(newNext, INTSIZE, 1, f);
        }
        void writePrev(int *newPrev, int address){
            fseek(f, address + TSIZE, SEEK_SET);
            fwrite(newPrev, INTSIZE, 1, f);
        }
        void writeFirstFree(int address){
            fseek(f, INTSIZE, SEEK_SET);
            fwrite(&address, INTSIZE, 1, f);
        }

        int getFirstFree(){
            int ret;
            fseek(f, INTSIZE, SEEK_SET);
            fread(&ret, INTSIZE, 1, f);
            return ret;
        }

    public:
        typedef T value_type;

        class const_iterator{
            private:
                FILE *fi;
                int address;
                const int TSIZE = sizeof(T);
                T getDataFromFI(int address) const{
                    T ret;
                    fseek(fi, address, SEEK_SET);
                    fread(&ret, TSIZE, 1, fi);
                    return ret;
                }

                int getPrevFromFI(int address) const{
                    int spot = address + TSIZE;
                    fseek(fi, spot, SEEK_SET);
                    int ret;
                    fread(&ret, INTSIZE, 1, fi);
                    return ret;
                }
                int getNextFromFI(int address) const{
                    int spot = address + TSIZE + INTSIZE;
                    fseek(fi, spot, SEEK_SET);
                    int ret;
                    fread(&ret, INTSIZE, 1, fi);
                    return ret;
                }
                int getAddressFromFI(int index) const{
                    int currentSpot = (INTSIZE*2);
                    for(int i = -1; i < index; i++)
                        currentSpot = getNextFromFI(currentSpot);
                    return currentSpot;
                }
  
            public:
                const_iterator(int i,FILE *fn){
                    fi = fn;
                    address = getAddressFromFI(i); 
                }
                const_iterator(FILE *fn, int newAddress){
                    fi = fn;
                    address = newAddress;
                }
                const_iterator(const const_iterator &i){
                    fi = i.fi;
                    address = i.address;
                }
                T operator*(){
                    return getDataFromFI(address);
                }
                bool operator==(const const_iterator &i) const{
                    return (address == i.address && fi == i.fi);
                }
                bool operator!=(const const_iterator &i) const{
                    return (address != i.address || fi != i.fi);
                }
                const_iterator &operator=(const const_iterator &i){
                    address = i.address;
                    fi = i.fi;
                    return *this;
                }
                const_iterator &operator++(){
                    address = getNextFromFI(address);
                    return *this;
                }
                const_iterator &operator--(){
                    address = getPrevFromFI(address);
                    return *this;
                }
                const_iterator operator++(int){
                    int temp = address;
                    address = getNextFromFI(address);
                    return const_iterator(fi, temp);
                }
                const_iterator operator--(int){
                    int temp = address;
                    address = getPrevFromFI(address);
                    return const_iterator(fi, temp);
                }
                

            friend class FileLinkedList;
        };

        const_iterator begin(){return const_iterator(f, getNext(SENTINEL));}
        const_iterator begin() const{return const_iterator(f, getNext(SENTINEL));}
        const_iterator end(){return const_iterator(f, SENTINEL);}
        const_iterator end() const{return const_iterator(f, SENTINEL);}
        const_iterator cbegin() const{return const_iterator(f, getNext(SENTINEL));}
        const_iterator cend() const{return const_iterator(f, SENTINEL);}

        FileLinkedList(const std::string &fname){
            f = fopen(fname.c_str(), "r+");
            if(f == nullptr){
                f = fopen(fname.c_str(), "w+");
                length = 0;
                
                int doubleIntSize = INTSIZE*2;
                writePrev(&doubleIntSize, doubleIntSize);
                writeNext(&doubleIntSize, doubleIntSize);
                
                writeFirstFree(-1);
            }else{
                fseek(f, 0, SEEK_SET);
                fread(&length, INTSIZE, 1, f);
            }
        }

        template<typename I>
        FileLinkedList(I begin,I end,const std::string &fname) {
            f = fopen(fname.c_str(), "r+");
            if(f == nullptr)
                f = fopen(fname.c_str(), "w+");
            else
                clear();
            length = 0;
            writeFirstFree(-1);
            int doubleIntSize = INTSIZE*2;
            writePrev(&doubleIntSize, doubleIntSize);
            writeNext(&doubleIntSize, doubleIntSize);    
            
            for(auto x = begin; x != end; x++)
                insert(cend(), *x);
        }

        ~FileLinkedList(){
            fseek(f, 0, SEEK_SET);
            fwrite(&length, INTSIZE, 1, f);
            fclose(f);
        }
        void push_back(const T &t){
            insert(cend(), t);
        }
        void pop_back(){
           erase(--cend()); 
        }
        int size() const{
            return length;
        }
        void clear(){
            while(length > 0)
                pop_back();
        }
        const_iterator insert(const_iterator position, const T &t){
            int currentNode = position.address;
            int prevNode = getPrev(currentNode);
            int spot = getFirstFree();
            if(spot == -1)
                spot = (INTSIZE * 2) + (sizeof(Node) * (length+1));
            else
                writeFirstFree(getNext(spot));

            writeData(&t, spot);
            writePrev(&prevNode, spot);
            writeNext(&currentNode, spot);

            writeNext(&spot, prevNode);
            writePrev(&spot, currentNode);
            length++;
            return const_iterator(f, spot);
        }
        T operator[](int index) const{
            return read(getAddress(index)).data;
        }
        const_iterator erase(const_iterator position){
            int loc = position.address;
            int nextLoc = getNext(loc);
            int prevLoc = getPrev(loc);
            
            writeNext(&nextLoc, prevLoc);
            writePrev(&prevLoc, nextLoc);
            length--;

            int firstFree = getFirstFree();
            writeNext(&firstFree, loc);
            writeFirstFree(loc);

            return const_iterator(f, nextLoc);
        }
        void set(const T &value, int index){
            writeData(&value, getAddress(index));
        }
        void set(const T &value, const_iterator position){
            writeData(&value, position.address);
        }
};

#endif
