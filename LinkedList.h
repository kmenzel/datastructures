//Karl Menzel
//Trinity University

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

template<typename T>
class LinkedList{
    struct Node{
        T data;
        Node *next;
        Node *prev;
    };
    
    private:
        Node *head;
        int length;
    public:
        typedef T value_type;
        class iterator{
            private:
                Node* point;
            public:
                iterator(Node *l){point = l;}
                iterator(){point = nullptr;}
                iterator(const iterator &i){point = i.point;}
                T &operator*() {return point->data;}
                bool operator==(const iterator &i) const {return (point == i.point);}
                bool operator!=(const iterator &i) const {return (point != i.point);}
                iterator &operator=(const iterator &i) {point = i.point;}
                iterator &operator++() {point = point->next; return *this;}
                iterator &operator--() {point = point->prev; return *this;}
                iterator operator++(int) {point = point->next; return iterator(point->prev);}
                iterator operator--(int) {point = point->prev; return iterator(point->next);}

                friend class const_iterator;
                friend class LinkedList;
        };

        class const_iterator{
            private:
                Node* point;
            public:
                const_iterator(Node *l){point = l;}
                const_iterator(){point = nullptr;}
                const_iterator(const const_iterator &i){point = i.point;}
                const_iterator(const iterator &i){point = i.point;}
                T &operator*() {return point->data;}
                bool operator==(const const_iterator &i) const {return (point == i.point);}
                bool operator!=(const const_iterator &i) const {return (point != i.point);}
                const_iterator &operator=(const const_iterator &i) {point = i.point;}
                const_iterator &operator++() {point = point->next; return *this;}
                const_iterator &operator--() {point = point->prev; return *this;}
                const_iterator operator++(int) {point = point->next; return const_iterator(point->prev);}
                const_iterator operator--(int) {point = point->prev; return const_iterator(point->next);}
        };

        iterator begin(){return iterator(head->next);}
        iterator end(){return iterator(head);}
        const_iterator begin() const{return cbegin();}
        const_iterator end() const{return cend();}
        const_iterator cbegin() const{return const_iterator(head->next);}
        const_iterator cend() const{return const_iterator(head);}
 
        LinkedList(){
            head = new Node();
            head->next = head;
            head-> prev = head;
            length = 0;
        }

        LinkedList(const LinkedList &al){
            head = new Node();
            head->next = head;
            head->prev = head;
            Node* traverse;
            traverse = al.head->next;
            while(traverse != al.head){
                push_back(traverse->data);
                traverse = traverse->next;
            }
            length = al.length; 
        }
        
        ~LinkedList(){
            clear();
            delete head;
        }

        LinkedList &operator=(const LinkedList &al){
            clear();
            head->next = head;
            head->prev = head;
            Node* traverse;
            traverse = al.head->next;
            while(traverse != al.head){
                push_back(traverse->data);
                traverse = traverse->next;
            }
            length = al.length;
            return *this;
        }
        
        void push_back(const T &t){
            insert(end(), t);
        }

        iterator insert(iterator position, const T &t){
            Node *temp;
            temp = new Node();
            temp->data = t;
            temp->next = position.point;
            temp->prev = position.point->prev;
            position.point->prev->next = temp;
            position.point->prev = temp;
            length++;
            return iterator(temp);
        }

        int size() const{
            return length;
        }

        void clear(){
            while(head->prev != head){
                pop_back();
            }
            length = 0;
        }

        void pop_back(){
            erase(--end());
        }
       
        iterator erase(iterator position){
            position.point->prev->next = position.point->next;
            position.point->next->prev = position.point->prev;
            length--;
            auto ret = position.point->next;
            delete position.point;
            return iterator(ret);
        }

        const T &operator[](int index) const{
            auto iter = begin();
            for(int i = 0; i < index; i++){
                iter++;
            }
            return *iter;
        }

        T &operator[](int index){
            auto iter = begin();
            for(int i = 0; i < index; i++){
                iter++;
            }
            return *iter; 
        }
};
#endif
