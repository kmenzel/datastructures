//Karl Menzel
//Trinity University

#ifndef FILEARRAYLIST_H
#define FILEARRAYLIST_H

#include <cstdio>
#include <string>

template<typename T>
class FileArrayList{
    FileArrayList(const FileArrayList<T> &that) = delete;
    FileArrayList<T> operator=(const FileArrayList<T> &that) = delete;

    private:
        FILE *f;
        int length;
        int SIZE_T;
        void write(int index, const T *t){
            fseek(f, (SIZE_T*index), SEEK_SET);
            fwrite(t, SIZE_T, 1, f);
        }
        T read(int index) const{
            T ret;
            fseek(f, (SIZE_T*index), SEEK_SET);
            fread(&ret, SIZE_T, 1, f);
            return ret;
        }
        T read(int index, FILE *fn) const{
            T ret;
            fseek(fn, (SIZE_T*index), SEEK_SET);
            fread(&ret, SIZE_T, 1, fn);
            return ret; 
        }
    public:
        typedef T value_type;

        class const_iterator {
            private:
                FILE *fi;
                int index;
                int SIZE_T;
            public:
                const_iterator(int i,FILE *fn){
                    fi = fn;
                    index = i;
                    SIZE_T = sizeof(T);
                }
                const_iterator(const const_iterator &i){
                    index = i.index;
                    fi = i.fi;
                    SIZE_T = sizeof(T);
                }
                T operator*(){
                    T ret;
                    fseek(fi, (SIZE_T*index), SEEK_SET);
                    fread(&ret, SIZE_T, 1, fi);
                    return ret; 
                }
                bool operator==(const const_iterator &i) const{
                    return (index == i.index && fi == i.fi);
                }
                bool operator!=(const const_iterator &i) const{
                    return (index != i.index || fi != i.fi);
                }
                const_iterator &operator=(const const_iterator &i){
                    index = i.index;
                    fi = i.fi;
                    return *this;
                }
                const_iterator &operator++(){
                    index++;
                    return *this;
                }
                const_iterator &operator--(){
                    index--;
                    return *this;
                }
                const_iterator operator++(int){
                    index++;
                    return const_iterator(index-1, fi);
                }
                const_iterator operator--(int){
                    index--;
                    return const_iterator(index+1, fi);
                }

                friend class FileArrayList;
        };
 
        const_iterator begin(){return cbegin();}
        const_iterator begin() const{return cbegin();}
        const_iterator end(){return cend();}
        const_iterator end() const{return cend();}
        const_iterator cbegin() const{return const_iterator(0, f);}
        const_iterator cend() const{return const_iterator(length, f);} 

        FileArrayList(const std::string &fname){
            SIZE_T = sizeof(T);
            f = fopen(fname.c_str(), "r+");
            if(f == nullptr){
                f = fopen(fname.c_str(), "w+");
                length = 0;
            } else {
                fseek(f, 0, SEEK_END);
                length = (ftell(f)/SIZE_T);
            }
        }

        template<typename I>
        FileArrayList(I begin,I end,const std::string &fname) {
            SIZE_T = sizeof(T);
            f = fopen(fname.c_str(), "r+");
            if(f == nullptr){
                f = fopen(fname.c_str(), "w+");
                length = 0;
            } else
                clear();
 
            for(auto x = begin; x != end; x++)
                push_back(*x);
        }

        ~FileArrayList(){
            fclose(f); 
        }
        void push_back(const T &t){
            insert(cend(), t);
        }
        void pop_back(){
            length--;
        }
        int size() const{
            fseek(f, 0, SEEK_END);
            int seeked = ftell(f);
            int i = ftell(f);
            return length;
        }
        void clear(){
            while(length > 0)
                pop_back();
        }
        const_iterator insert(const_iterator position, const T &t){
            int index = position.index;
            for(int i = length; i > index; i--){
                T temp = read(i-1);
                write(i, &temp);
            }
            write(index, &t);
            length++;
            return const_iterator(index, f); 
        }
        T operator[](int index) const{
            return read(index);
        }
        const_iterator erase(const_iterator position){
            int index = position.index;
            for(int i = index; i < length-1; i++){
                T temp = read(i+1);
                write(i, &temp);
            }
            length--;
            return const_iterator(index, f);
        }
        void set(const T &value,int index){
            write(index, &value);
        }       
};

#endif
